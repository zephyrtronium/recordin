/**
 * Copyright 2022 Branden J Brown
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
)

func main() {
	var a, echo bool
	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "%s [-a] [-echo] <output-script> <command> [args]\n", os.Args[0])
		flag.PrintDefaults()
	}
	flag.BoolVar(&a, "a", false, "append to existing script")
	flag.BoolVar(&echo, "echo", false, "print command being executed to stderr before running")
	flag.Parse()

	log.SetFlags(0)
	log.SetPrefix("recordin: ")
	if len(flag.Args()) < 2 {
		flag.Usage()
		return
	}

	mode := os.O_WRONLY | os.O_CREATE | os.O_TRUNC
	if a {
		// Toggle off O_TRUNC.
		mode ^= os.O_APPEND | os.O_TRUNC
	}
	out, err := os.OpenFile(flag.Arg(0), mode, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err := out.Close(); err != nil {
			log.Print(err)
		}
	}()

	cmd := exec.Command(flag.Arg(1), flag.Args()[2:]...)
	cmd.Stdin = &inputTee{in: os.Stdin, out: out}
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if echo {
		log.Println(cmd.String())
	}
	if err := cmd.Run(); err != nil {
		log.Println(err)
	}
}

type inputTee struct {
	in  io.Reader
	out io.Writer
}

func (i *inputTee) Read(p []byte) (int, error) {
	n, err := i.in.Read(p)
	p = p[:n]
	i.copy(p)
	return n, err
}

func (i *inputTee) copy(p []byte) {
	n, err := i.out.Write(p)
	if err != nil {
		log.Println("wrote", n, "of", len(p), "bytes:", err)
	}
}
