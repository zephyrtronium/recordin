# Recordin

Recordin records standard input to a separate file while running a command.

After recording, the file can be supplied as standard input to the same command to replay the session:

```
recordin stdin.txt some-command arg0 arg1
some-command arg0 arg1 <stdin.txt
```

## Usage

```
recordin [-a] [-echo] <output-script> <command> [args]
  -a    append to existing script
  -echo
        print command being executed to stderr before running
```

`<output-script>` is the file to which to record standard input. If `-a` is supplied, the recording is appended to the file if it exists; otherwise, the file is overwritten.
